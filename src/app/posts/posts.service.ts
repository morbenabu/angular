import { Injectable } from '@angular/core';
//bring http's element that exists in angular
import {Http} from '@angular/http';
//to change the data from the server to json insted string
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';
//כדי שאלמנט הספינר יפעל יש להוסיף אימפורט של דיליי
import 'rxjs/add/operator/delay';

@Injectable()
export class PostsService {
  // private _url = 'http://jsonplaceholder.typicode.com/posts';

  postsObservable;
  getPosts(){
    // return this._http.get(this._url).map(res=> res.json()).delay(2000);
    //this.postsObservable = this.af.database.list('/posts');
    this.postsObservable = this.af.database.list('/posts').map(
      posts =>{
        posts.map(
          post => {
            post.userNames = [];
            for(var p in post.users){
                post.userNames.push(
                this.af.database.object('/users/' + p)
              )
            }
          }
        );
        return posts;
      }
    )
   return this.postsObservable; 
 // let posts= [
   //   {author:'mor',content:'abcdefg', title:'titleA'},
     // {author:'tal',content:'123456', title:'titleB'},
      //{author:'daniel',content:'a1b2c3d4', title:'titleC'},
      //{author:'almog',content:'mmmmmm', title:'titleE'}
 
   // ]
   // return posts;
  }

addPost(post){
    this.postsObservable.push(post);
  }
 updatePost(post){
   let postKey = post.$key;
    let postData = {title : post.title, body: post.body};
    this.af.database.object('/posts/' + postKey).update(postData);
  }

   deletePost(post){
    let postKey = post.$key;
    this.af.database.object('/posts/' + postKey).remove();
  }

  //constructor(private _http:Http) { }
  constructor(private af:AngularFire) { }

}
