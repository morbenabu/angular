import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';

@Component({
  selector: 'post-posts',
  templateUrl: './posts.component.html',
  //styleUrls: ['./posts.component.css']
  styles: [`
     .posts li { cursor: default; }
    .posts li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }      
  `]
})
export class PostsComponent implements OnInit {

  isLoading:Boolean = true;

  posts;
  currentPost;

addPost(post){
    //this.posts.push(post)
    this._postsService.addPost(post); 
  }

   updatePost(post){
    this._postsService.updatePost(post);
   }

   select(post){
 		this.currentPost = post; 
//Add a comment to this line
    // console.log(	this.currentPost);
  }
  
   constructor(private _postsService: PostsService) {
   // this.posts = this._postsService.getPosts();
  }

  //editPost(post){
    //this.posts.splice(
      //this.posts.indexOf(post),0
    //)
  //}

  deletePost(post){
    //this.posts.splice(
      //this.posts.indexOf(post),1
    //)
    this._postsService.deletePost(post);
  }

  ngOnInit() {
     this._postsService.getPosts().subscribe(postsData=>
      {this.posts =postsData;
        this.isLoading = false;
         console.log(this.posts)});
  }

}
