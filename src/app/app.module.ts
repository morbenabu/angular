import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import{RouterModule, Routes } from '@angular/router';
import {AngularFireModule} from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
//import { DemoComponent } from './demo/demo.component';
import { PostsComponent } from './posts/posts.component';
import { PostComponent } from './post/post.component';

import { PostsService } from './posts/posts.service';
import { UsersService } from './users/users.service';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UserComponent } from './user/user.component';
import { PostFormComponent } from './post-form/post-form.component';

export const firebaseConfig = {
    apiKey: "AIzaSyA2E7yfgEHdaBVyiPx5pcZkG0Csao9JG0A",
    authDomain: "angular-70057.firebaseapp.com",
    databaseURL: "https://angular-70057.firebaseio.com",
    storageBucket: "angular-70057.appspot.com",
    messagingSenderId: "789411969464"
  }

const appRoutes:Routes =[
  {path:'users', component: UsersComponent}, 
  {path:'posts', component: PostsComponent},
  {path:'', component: UsersComponent},
  {path:'**', component: PageNotFoundComponent}
]


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
   // DemoComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    UserFormComponent,
    UserComponent,
    PostFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule, 
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [PostsService, UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
