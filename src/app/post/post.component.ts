import { Component, OnInit, EventEmitter, Output  } from '@angular/core';
//bring the new class Post from post.ts
import {Post} from './post';


@Component({
  selector: 'post-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})
export class PostComponent implements OnInit {
post:Post;
@Output() deleteEvent = new EventEmitter<Post>();
@Output() editEvent = new EventEmitter<Post>();

isEdit:Boolean = false; 
editButtonText = "Edit";

//newField = [];
//cancel;
//CancelEdit= "Cancel";
//editing:Boolean = false;
bodyOld;
titleOld;

  constructor() { }
  sendDelete(){ 
    this.deleteEvent.emit(this.post);
  }

  toggleEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = "Save" : this.editButtonText = "Edit" ;
    this.isEdit ? this.saveOldPost() : this.saveNewPost();
  }

  saveOldPost(){
    this.bodyOld=this.post.body;
    this.titleOld=this.post.title;
  }
  saveNewPost(){
    this.editEvent.emit(this.post)
  }
  sendCancel(){
    this.post.title = this.titleOld;
    this.post.body = this.bodyOld;
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = "Save" : this.editButtonText = "Edit" ;
  }
  

  //sendCancel(){
    //this.cancel = function(index) {
    //if (this.editing !== false) {
       // this.appkeys[this.editing] = this.newField;
        //this.editing = false ;
   // }      
//};
 // }

  ngOnInit() {
  }

}
